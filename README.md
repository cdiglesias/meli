# Mercadolibre test
----
## Main Process
To run this process, execute this command

```sh
$ node meli.js [OPTIONS]
```
```sh
OPTIONS:  
--Ferengi=<offset>      Ferengi offset in initial day (in degree)  
--Betasoide=<offset>    Betasoide offset in initial day (in degree)  
--Vulcano=<offset>      Vulcano offset in initial day (in degree)  
--help                  Read this command options  
```

## Job Process
To process the weather for each day and save them, run this command
```sh
$ node job.js [OPTIONS]
```
```sh
OPTIONS:  
--Ferengi=<offset>      Ferengi offset in initial day (in degree)  
--Betasoide=<offset>    Betasoide offset in initial day (in degree)  
--Vulcano=<offset>      Vulcano offset in initial day (in degree)  
--help                  Read this command options  
```
This task will clean all database, and will create all record regarding to each days: from day 1 to day 3650
  
## Frameworks & Utils
  
This implementation is using [nodejs] to solve this problem, with:
- [ExpressJs]
- [Mongoose]
 

   [nodejs]: <http://nodejs.org/>
   [ExpressJs]: <http://expressjs.com/>
   [Mongoose]: <http://mongoosejs.com/>


License
----

MIT
