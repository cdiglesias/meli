var args = process.argv.slice(2),
    offset = {},
    mode = args.shift();

if (args || !mode || !(mode === 'print' || mode === 'job')) {
    if (args[0] == '--help' || !mode || !(mode === 'print' || mode === 'job')) {
        console.log('node meli.js print|job [OPTIONS]\n');
        console.log('modes:');
        console.log('   print                Print all result on screen');
        console.log('   job                  Clear all data from database, and persist all days on db');
        console.log('\nOPTIONS:');
        console.log('--Ferengi=<offset>      Ferengi offset in initial day (in degree)');
        console.log('--Betasoide=<offset>    Betasoide offset in initial day (in degree)');
        console.log('--Vulcano=<offset>      Vulcano offset in initial day (in degree)');
        console.log('--help                  Read this command options');
        return;
    }

    for (var i = 0; i < args.length; i++) {
        var param = args[i].split('=');
        if (param.length > 1) {
            param[1] = parseInt(param[1], 10);
            if (!isNaN(param[1]))
                offset[param[0].replace('--', '')] = param[1];
        }
    }
}

var Process = require('./app/Process'),
    result = Process.run(offset);

if (mode === 'job') {
    var Persist = require('./app/Persist');

    Persist.save(result, function(err) {
        if (!err) {
            console.log('Job executed successfull');
        }
        else {
            console.log('Error on ' + err);
        }
    });
}
else if (mode === 'print') {

    // drought
    console.log('----------------\nDías de sequía\n----------------');
    console.log(result.drought.join(', '));

    // rain
    var rainList = [],
        key;

    for (key in result.rain)
        rainList.push(result.rain[key]);
    console.log('\n----------------\nDías de lluvia\n----------------');
    console.log(rainList.join(', '));

    console.log('\n----------------\nDías de máxima lluvia\n----------------');
    console.log(result.rain[key].join(', '));

    console.log('\n----------------\nCondiciones óptimas de presión y temperatura\n----------------');
    console.log(result.optimum.join(', '));
}
