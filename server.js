var http = require('http');
var express = require('express');

var router = express();
var server = http.createServer(router);

var Process = require('./app/Process');

router.get('/clima', function(req, res) {
  var day = parseInt(req.param('dia'), 10);

  if (isNaN(day)) {
    res.status(400).send('Format day is incorrect');
    return;
  }

  res.json(Process.day(day));
});

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function() {
  var addr = server.address();
  console.log("Run Meli test Server", addr.address + ":" + addr.port);
});
