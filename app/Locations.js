var fs = require('fs');

var cache = {
    file: './cache/location.json',
    config: null,
    data: {},
    counter: 0,
    __persist: function() {
        this.counter++;

        if (this.counter > 10) {
            fs.writeFile(this.file, JSON.stringify(this.data));
            this.counter = 0;
        }
    },
    load: function() {
        if (fs.existsSync(this.file)) {
            this.data = JSON.parse(fs.readFileSync(this.file));
            this.counter = 0;
        }
    },
    planet: function(config) {
        if (!this.data[config.moving]) {
            this.data[config.moving] = {};
            this.__persist();
        }
        this.config = config;
        return this;
    },
    position: function(day, position) {
        if (position !== undefined) {
            this.data[this.config.moving][day] = position;
            this.__persist();
        }

        return this.data[this.config.moving][day];
    }
};

cache.load();

function toRadians(angle) {
    return angle * (Math.PI / 180);
}

exports.getLocation = function(config, day) {

    var angle = (config.moving * day + config.offset) % 360;
    if (angle < 0)
        angle = 360 + angle;

    var planet = cache.planet(config),
        position = planet.position(angle);

    if (!position) {
        position = planet.position(angle, this.calculatePosition(config, angle));
    }

    return position;
};

exports.calculatePosition = function(config, angle) {
    angle = toRadians(angle);
    var x = Math.cos(angle) * config.distance;
    var y = Math.sin(angle) * config.distance;

    return {
        x: x,
        y: y
    };
};
