exports.getLine = function(pA, pB, __n) {
    var slope = (pB.y - pA.y) / (pB.x - pA.x);

    return {
        contain: function(p, n) {
            var y = slope * (p.x - pA.x) + pA.y;
            n = (n || __n || 5) / 100;

            return Math.abs(p.y - y) <= n * y;
        }
    };
};

exports.getTriangleContain = function(pA, pB, pC, pContain) {
    function sign(p1, p2, p3) {
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
    }

    var b1, b2, b3;

    b1 = sign(pContain, pA, pB) < 0;
    b2 = sign(pContain, pB, pC) < 0;
    b3 = sign(pContain, pC, pA) < 0;

    return ((b1 == b2) && (b2 == b3));
};

function distance(p1, p2) {
    return Math.round(Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2)));
}

exports.getTrianglePerimeter = function(pA, pB, pC) {
    return distance(pA, pB) + distance(pA, pC) + distance(pB, pC);
};