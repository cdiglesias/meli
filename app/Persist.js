var mongoose = require('mongoose'),
	db = 'mongodb://meli:meli@ds011228.mongolab.com:11228/heroku_dqjd3hv5';

var Weather = mongoose.model('Weather', {
	day: Number,
	type: String
});

var con = mongoose.createConnection(db);
con.once('open', function() {

});

exports.save = function(result, cb){ 
	var type, key, record = {},
		Flag = require('./Flag'),
		flag = new Flag(function(errors) {
			for (var i in errors)
				if (errors[i]) {
					cb(errors[i]);
					return;
				}

			cb();
		});

	for (type in result) {
		record.type = type;

		for (key in result[type]) {
			if (type === 'rain') {
				for (var perimeter in result[type][key]) {
					record.day = result[type][key][perimeter];
					flag.up();
					new Weather(record).save(function(err) {
						flag.down(err);
					});
				}
			}
			else {
				record.day = result[type][key];
				flag.up();
				new Weather(record).save(function(err) {
					flag.down(err);
				});
			}
		}
	}
};