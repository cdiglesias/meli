var Locations = require('./Locations'),
    Geometry = require('./Geometry'),
    planets = require('../config/planets.json'),
    sun = {
        x: 0,
        y: 0
    };

exports.run = function(offset) {
    if (offset) {
        planets['Ferengi'].offset = offset['Ferengi'] || planets['Ferengi'].offset;
        planets['Betasoide'].offset = offset['Betasoide'] || planets['Betasoide'].offset;
        planets['Vulcano'].offset = offset['Vulcano'] || planets['Vulcano'].offset;
    }

    var resultList = {
        rain: {},
        drought: [],
        optimum: []
    };

    for (var day = 1; day <= 3650; day++) {
        var result = exports.day(day);

        if (result.type === 'drought')
            resultList.drought.push(result.day);

        else if (result.type === 'optimum')
            resultList.optimum.push(result.day);

        else if (result.type === 'rain') {
            if (!resultList.rain[result.perimeter])
                resultList.rain[result.perimeter] = [];

            resultList.rain[result.perimeter].push(day);
        }
    }

    return resultList;
};

exports.day = function(day) {
    var location = {};
    location['Ferengi'] = Locations.getLocation(planets['Ferengi'], day);
    location['Betasoide'] = Locations.getLocation(planets['Betasoide'], day);
    location['Vulcano'] = Locations.getLocation(planets['Vulcano'], day);

    var line = Geometry.getLine(location['Ferengi'], location['Betasoide']),
        type = 'Unknow',
        perimeter;

    if (line.contain(location['Vulcano'])) {
        if (line.contain(sun))
            type = 'drought';
        else
            type = 'optimum';
    }
    else if (Geometry.getTriangleContain(location['Ferengi'], location['Betasoide'], location['Vulcano'], sun)) {
        type = 'rain';
        perimeter = Geometry.getTrianglePerimeter(location['Ferengi'], location['Betasoide'], location['Vulcano']);

    }

    return {
        day: day,
        type: type,
        perimeter
    };
};