function Flag(fnEnd) {
    this.fnEnd = fnEnd;
    this.counter = 0;
    this.notExec = true;
    this.result = [];

    this.verified = function() {
        if (this.counter === 0 && this.fnEnd !== undefined && this.notExec) {
            this.fnEnd(this.result);
            this.notExec = false;
        }
    };
}

Flag.prototype.up = function() {
    this.counter++;
    this.verified();
};

Flag.prototype.down = function(result) {
    this.counter--;
    this.result.push(result);
    this.verified();
};

module.exports = Flag;